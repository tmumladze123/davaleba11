package com.example.splashsplash

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.splashsplash.databinding.FragmentRecyclerBinding


class recyclerFragment : Fragment() {
    private lateinit var adapter:pictureAdapter
    private  var binding: FragmentRecyclerBinding? =null
    var Picture= mutableListOf<picture>(
        picture("cat","desc",R.mipmap.cat),
        picture("cat","desc",R.mipmap.cat2),
        picture("cat","desc",R.mipmap.cat3),
        picture("cat","desc",R.mipmap.cat4),
        picture("dog","desc",R.mipmap.dog1),
        picture("dog","desc",R.mipmap.dog2),
        picture("dog","desc",R.mipmap.dog3),
        picture("dog","desc",R.mipmap.dog4))
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding=FragmentRecyclerBinding.inflate(inflater,container,false)
        Listener()
        search()
        return binding!!.root
    }
    fun Listener()
    {
        adapter=pictureAdapter()
        adapter.fillNumbers(Picture)
        adapter.call={
            changeFragment(it)
        }
        binding?.rvPictures?.adapter=adapter
        binding?.rvPictures?.layoutManager= GridLayoutManager(context,2)


    }
    fun changeFragment(picture:Int)
    {
        findNavController().navigate(recyclerFragmentDirections.actionRecyclerFragmentToViewItem())

    }
    fun search()
    {   var newList= mutableListOf<picture>()
        binding?.searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                for( picture in Picture)
                {
                    if(picture.name.equals(query) || query?.let { picture.desc.contains(it) } == true)
                    {
                        newList.add(picture)
                    }
                }
                if(newList.isEmpty())
                {
                    adapter.filterList(newList)
                    adapter.notifyDataSetChanged()
                    return true
                }

                else

                    return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                for( picture in Picture)
                {
                    if(newText?.let { picture.name.contains(it) } == true || newText?.let { picture.desc.contains(it) } == true)
                    {
                        newList.add(picture)
                    }
                }
                if(newList.isEmpty())
                    return false
                else
                {
                    adapter.filterList(newList)
                    adapter.notifyDataSetChanged()
                    return true
                }

            }

        })
    }

}