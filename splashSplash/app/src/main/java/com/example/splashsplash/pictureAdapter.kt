package com.example.splashsplash

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.splashsplash.databinding.OneItemBinding

typealias callBack = (Int) -> Unit

class pictureAdapter () :  RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    private  var picture= mutableListOf<picture>()
    public lateinit var call: callBack
    inner class ViewHolder(var binding: OneItemBinding) :RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        override fun onClick(p0: View?) {
            call.invoke(picture[adapterPosition].picture)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as ViewHolder
        holder.binding.tvName.text=picture[position].name
        holder.binding.tvDesc.text=picture[position].desc
        holder.binding.pic.setImageResource(picture[position].picture)

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder
            =ViewHolder(OneItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))

    override fun getItemCount(): Int = picture.size
    public fun fillNumbers(picturessList: MutableList<picture>)
    {
        picture.addAll(picturessList)
        notifyDataSetChanged()
    }
    public fun filterList(picturessList: MutableList<picture>)
    {
        picture.clear()
        picture.addAll(picturessList)
    }

}
