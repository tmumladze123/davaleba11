package com.example.splashsplash

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class picture (var name:String, var desc:String, var picture:Int) : Parcelable