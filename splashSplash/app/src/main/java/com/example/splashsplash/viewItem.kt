package com.example.splashsplash

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.splashsplash.databinding.FragmentRecyclerBinding
import com.example.splashsplash.databinding.FragmentViewItemBinding


class viewItem : Fragment() {
    private  var binding: FragmentViewItemBinding? =null
    private val args: viewItemArgs by navArgs()
    override fun onCreateView(

        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding=FragmentViewItemBinding.inflate(inflater,container,false)
        setPicture()
        return binding!!.root
    }
    fun setPicture()
    {
        binding?.pic?.setImageResource(args.picture)
    }


}